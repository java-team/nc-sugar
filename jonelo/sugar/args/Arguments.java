/******************************************************************************
 *
 * Sugar for Java 2.0.0
 * Copyright (C) 2001-2006  Dipl.-Inf. (FH) Johann Nepomuk Loefflmann,
 * All Rights Reserved, http://www.jonelo.de
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * is a GPL'd argument processing class that makes argument processing easy,
 * but also something that wouldn't get in the way (based off Arguments 1.1:
 * A C++ Argument Processing Class, by Jared Davis, http://www.opendeli.org/arguments).
 *
 * @author Spencer Gibb (sbg2@email.byu.edu)
 *
 * @author jonelo (jonelo@jonelo.de)
 * - moved Arguments to jonelo's sugar package
 * - added on/off and enable/disable to truthOf()
 * - added a constructor instead of using the static method
 *   setArgumentsWithSpaces()
 *
 *****************************************************************************/
package jonelo.sugar.args;

import java.util.Vector ;
import java.util.Hashtable ;
import java.util.StringTokenizer ;

public class Arguments {
    private static final String colon = ":" ;
    private static final String equal = "=" ;
    
    /**
     * Holds all the arguments that can be entered with a space (ie -geom 500+2)
     */
    private Vector spacesArgs;
    
    /**
     * Holds all the argument namesprivate Vector args;
     */
    private Vector args;
    
    /**
     * Holds all the argument values
     */
    private Vector values;
    
    /**
     * Tells the Argument class which arguments can take the space style args (ie -name value).
     * @param arguments A "|" delimited String of arguments (ie "--name|/host")
     */
    public Arguments(String[] argv, String arguments) {
        spacesArgs = new Vector() ;
        StringTokenizer st = new StringTokenizer(arguments, "|");
        while ( st.hasMoreTokens() ) {
            spacesArgs.add( st.nextToken() ) ;
        }
        constructor(argv);
    }
    
    /**
     * Performs all internal initializations of arguments
     * @param argv an array of strings, should be the same array given to main
     */
    public Arguments( String[] argv ) {
        constructor(argv);
    }
    
    /**
     * Constructor method
     * @param argv an array of strings, should be the same array given to main
     */
    private void constructor(String[] argv) {
        args = new Vector() ;
        values = new Vector() ;
        
        for(int i=0; i < argv.length; i++) {
            
            String arg = argv[i] ;
            
            int sepIndex = arg.indexOf(colon) ;
            
            if ( sepIndex == -1 && arg.indexOf(equal) > -1 )
                sepIndex = arg.indexOf(equal) ;
            
            if ( sepIndex != -1 ) {
                args.add( arg.substring(0, sepIndex ) ) ;
                values.add( arg.substring(sepIndex+1) ) ;
            } else if ( spacesArgs.indexOf( arg ) > -1 ) { // check to see if it's in the spaces list
                String argValue = "" ;
                if ( i < argv.length - 1 ) {
                    argValue = argv[i+1] ;
                    i++ ;
                }
                args.add( arg ) ;
                values.add( argValue ) ;
            } else {
                // Otherwise, we have not found a ':' or an '='.
                // Add arg to args, and add a null string to d_vals.
                args.add( arg ) ;
                values.add( "" ) ;
            }
        }
    } // end Arguments
    
    
    /**
     *  Attempts to return a bool value from an argument.
     *  <br>Example Arguments: --something=true, --check=no
     *  <br>Recognizes any capitalization of: Yes, No, Y, N, True, False, T, F, 1, or 0.
     *
     * @param arg the name of the argument
     * @return A boolean, whether the argument was true or false
     * @throws jonelo.sugar.args.InvalidArgumentException if the argument is non-existant or has a bad boolean value
     */
    public boolean getBooleanValueOf( String arg )
    throws InvalidArgumentException {
        if ( !receivedArgument(arg) )
            throw new InvalidArgumentException( "Invalid Argument: " + arg ) ;
        else
            return truthOf( getValueOf(arg) ) ;
    }
    
    /**
     *  Attempts to return a string value from an argument.
     *  Example Arguments: -I=../Includes, -o my-file, -host=192.168.1.1
     * @param arg the name of the argument
     * @return Returns the value or an empty string if argument was not passed a value.
     * @throws org.opendeli.InvalidArgumentException if the argument is non-existant or has a bad boolean value
     */
    public String getStringValueOf( String arg )
    throws InvalidArgumentException {
        if ( !receivedArgument(arg) )
            throw new InvalidArgumentException( "Invalid Argument: " + arg ) ;
        else
            return getValueOf(arg) ;
    }
    
    
    /**
     *  Attempts to return a double value from an argument.
     *  <br>Example Arguments: --debug-level=5, --precision 20, --pi=3.1415
     *  <br>Recognizes any number made up of digits 0-9 and (at most) one decimal point.
     * @param arg the name of the argument
     * @return Returns double value from an argument.
     * @throws org.opendeli.InvalidArgumentException if the argument is non-existant or has a bad boolean value
     */
    public Double getDoubleValueOf( String arg )
    throws InvalidArgumentException {
        if ( !receivedArgument(arg) )
            throw new InvalidArgumentException( "Invalid Argument: " + arg ) ;
        else
            return Double.valueOf( getValueOf(arg) ) ;
    }
    
    /**
     * @return Returns true if arg was in argv, false if not.
     */
    public boolean receivedArgument( String arg ) {
        return ( args.indexOf(arg) > -1 ) ;
    }
    
    /**
     * @return Returns a vector of arguments
     */
    public Vector listArguments() {
        return args ;
    }
    
    /**
     *  Returns a string representation of all the arguments with thier values.
     * @return Returns a string representation of all the arguments with thier values.
     * <br>Example { /name=value -name2=value2 }
     */
    public String toString() {
        String result = "{ " ;
        for (int i=0; i< args.size(); i++) {
            result += args.get(i) + "="  + values.get(i) + " " ;
        }
        result += "}" ;
        
        return result ;
    }
    
    private String getValueOf( String arg ) {
        return (String)values.get(args.indexOf(arg)) ;
    }
    
    private boolean truthOf( final String val )
    throws InvalidArgumentException
            // Returns true or false if the argument is true or false, throw an exception if there's a prob.
            // Ugly looking, but it works and stuff.
    {
        // Val is only one character.  It can be yes or no...
        if (    val.substring(0,1).equals("1") ||
                val.substring(0,1).toLowerCase().equals("y") ||
                val.substring(0,1).toLowerCase().equals("t") )
            return true ;
        else if (
                val.substring(0,1).equals("0") ||
                val.substring(0,1).toLowerCase().equals("n") ||
                val.substring(0,1).toLowerCase().equals("f") )
            return false ;
        else if ( val.toLowerCase().equals("no") ||
                val.toLowerCase().equals("false") ||
                val.toLowerCase().equals("off") ||
                val.toLowerCase().equals("disable")
                )
            return false;
        
        else if ( val.toLowerCase().equals("yes") ||
                val.toLowerCase().equals("true") ||
                val.toLowerCase().equals("on") ||
                val.toLowerCase().equals("enable")
                )
            return true;
        
        else
            throw new InvalidArgumentException( "Invalid Boolean Value in Argument: " + val );
    }
    
    
} // end class Arguments
