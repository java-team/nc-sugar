/******************************************************************************
 *
 * Sugar for Java 2.0.0
 * Copyright (C) 2001-2006  Dipl.-Inf. (FH) Johann Nepomuk Loefflmann,
 * All Rights Reserved, http://www.jonelo.de
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @author jonelo@jonelo.de
 *
 * 11-Oct-2003 ls with and without suffix
 *
 *****************************************************************************/
package jonelo.sugar.io;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.DataFlavor;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

public class GeneralIO {

    public final static String ERROR_DIRNOTFOUND = "Error.DirNotFound",
            ERROR_NOTADIR = "Error.NotADirectory",
            ERROR_READDIR = "Error.CanNotReadDirectory";
    public static String ERROR_CLIPBOARD_EMPTY = "Clipboard is empty.",
            ERROR_CLIPBOARD_ISNOTTEXT = "Content of clipboard isn't text.";

    /** Creates new GeneralIO */
    public GeneralIO() {
    }

    public static Vector ls(String directory, String suffix) throws Exception {
        return ls(directory, suffix, true);
    }

    public static Vector ls(String directory, String suffix, boolean withSuffix) throws Exception {
        File present = new File(directory);

        if (!present.exists()) {
            throw new Exception(ERROR_DIRNOTFOUND + " " + directory);
        }
        if (!present.isDirectory()) {
            throw new Exception(ERROR_NOTADIR + " " + directory);
        }
        if (!present.canRead()) {
            throw new Exception(ERROR_READDIR + " " + directory);
        }

        int ind;
        Vector v = new Vector();
        String[] dirlist;

        for (dirlist = present.list(), ind = 0, Arrays.sort(dirlist); dirlist != null && ind < dirlist.length; ind++) {
            // jeden Eintrag f�r sich behandeln
            File entry = new File(present, dirlist[ind]);
            if (entry.isFile() && (dirlist[ind].endsWith(suffix))) {
                if (withSuffix) {
                    v.add(entry.getName());
                } else { // not suffix
                    v.add(entry.getName().substring(0, entry.getName().length() - suffix.length()));
                }
            }
        }
        if (v.size() == 0) {
            v = null;
        }
        return v;
    }

    public static Vector readFromJarInVector(Class myclass, String filename, String charsetName) throws Exception {
        Vector v = new Vector();
        InputStream is = myclass.getResourceAsStream(filename);
        BufferedReader br = new BufferedReader(new InputStreamReader(is, charsetName));
        String thisLine;
        StringBuilder sb = new StringBuilder();
        while ((thisLine = br.readLine()) != null) {
            v.add(thisLine);
        }
        br.close();
        return v;
    }

    public static Vector readFromJarInVector(Class myclass, String filename) throws Exception {
        return readFromJarInVector(myclass, filename, "ISO-8859-1");
    }

    /**
     * put a String to the clipboard
     * @param s the String
     */
    public static void setClipboard(String s) {
        if (s != null) {
            Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
            StringSelection content = new StringSelection(s);
            cb.setContents(content, null); //this
        }
    }

    public static String getClipboard() throws Exception {
        Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
        String s = null;
        Transferable content = cb.getContents(null);
        if (content == null) {
            throw new Exception(ERROR_CLIPBOARD_EMPTY);
        } else {
            try {
                s = (String) content.getTransferData(DataFlavor.stringFlavor);
            } catch (Exception e) {
                throw new Exception(ERROR_CLIPBOARD_ISNOTTEXT);
            }
        }
        return s;
    }

    public static void copyProperties(Properties fromProps, Properties toProps) {
        Enumeration enumeration = fromProps.propertyNames();
        while (enumeration.hasMoreElements()) {
            String key = (String) enumeration.nextElement();
            toProps.setProperty(key, fromProps.getProperty(key));
        }
    }
}
