/******************************************************************************
 *
 * Sugar for Java 2.0.0
 * Copyright (C) 2001-2006  Dipl.-Inf. (FH) Johann Nepomuk Loefflmann,
 * All Rights Reserved, http://www.jonelo.de
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @author jonelo@jonelo.de
 *
 *****************************************************************************/

package jonelo.sugar.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JWindow;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

public class SplashScreen extends JWindow {
  JLabel statusBar;

  public SplashScreen(ImageIcon icon){
    //super(new JFrame());
    setVisible(false);
    addMouseListener(new java.awt.event.MouseAdapter() {
      public void mouseClicked(java.awt.event.MouseEvent evt) {
         setVisible(false);
      }
    });

    JPanel panel=new JPanel(new BorderLayout());
    JLabel label = new JLabel(icon);
    label.setBorder(BorderFactory.createLineBorder(Color.black,1));

    statusBar=new JLabel(" ",SwingConstants.CENTER);
    statusBar.setFont(new Font("Dialog",Font.PLAIN, 12));
    statusBar.setForeground(Color.white);

    panel.setBackground(Color.black);
    panel.add(label,BorderLayout.CENTER);
    panel.add(statusBar,BorderLayout.SOUTH);
    getContentPane().add(panel, BorderLayout.CENTER);
    pack();
    GeneralGUI.centerComponent(this);
    setVisible(true);
    toFront();
  }

  public void showStatus(String currentStatus){
    try{
      SwingUtilities.invokeLater(new UpdateStatus(currentStatus));
    } catch(Exception e){e.printStackTrace();}
  }

  public void close(){
    try{
      SwingUtilities.invokeLater(new CloseSplashScreen());
    } catch(Exception e){e.printStackTrace();}
  }

  class UpdateStatus implements Runnable{
    String newStatus;

    public UpdateStatus(String status) {
      newStatus=status;
    }

    public void run() {
      statusBar.setText(newStatus);
    }
  }

  class CloseSplashScreen implements Runnable
  {
    public void run() {
      setVisible(false);
      dispose();
    }
  }
}
