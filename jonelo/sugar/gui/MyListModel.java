/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jonelo.sugar.gui;

import java.util.List;
import javax.swing.AbstractListModel;

public class MyListModel extends AbstractListModel {

    private final List<?> myList;

    public MyListModel(List<?> myList) {
        this.myList = myList;
    }

    @Override
    public int getSize() {
        return myList.size();
    }

    @Override
    public Object getElementAt(int index) {
        return myList.get(index);
    }
}