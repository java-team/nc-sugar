/**
 * ****************************************************************************
 *
 * Sugar for Java 2.0.0 Copyright (C) 2001-2006 Dipl.-Inf. (FH) Johann Nepomuk
 * Loefflmann, All Rights Reserved, http://www.jonelo.de
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author jonelo@jonelo.de
 *
 * 26-May-2002: added noTextChangeKeyEvents added maximizeComponent 20-Jul-2003:
 * added LAF_GTK 14-Oct-2003: dimension as singleton 09-May-2004: added
 * applyOrientation
 *
 ****************************************************************************
 */
package jonelo.sugar.gui;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.Vector;
import javax.swing.*;
import javax.swing.plaf.basic.BasicComboPopup;

public class GeneralGUI {

    private static java.awt.Dimension dimension = null; // singleton
    public final static String LAF_METAL = "javax.swing.plaf.metal.MetalLookAndFeel",
            LAF_MOTIF = "com.sun.java.swing.plaf.motif.MotifLookAndFeel",
            LAF_WINDOWS = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel",
            LAF_MAC = "com.sun.java.swing.plaf.mac.MacLookAndFeel", // Mac OS
            LAF_AQUA = "apple.laf.AquaLookAndFeel", // Mac OS X
            LAF_GTK = "com.sun.java.swing.plaf.gtk.GTKLookAndFeel"; // GTK (with J2SE 1.4.2)
    public final static int[] noTextChangeKeyEvents = {
        KeyEvent.VK_SHIFT, KeyEvent.VK_ENTER, KeyEvent.VK_INSERT,
        KeyEvent.VK_DOWN, KeyEvent.VK_UP, KeyEvent.VK_LEFT, KeyEvent.VK_RIGHT, // cursor
        KeyEvent.VK_HOME, KeyEvent.VK_END, KeyEvent.VK_PAGE_UP, KeyEvent.VK_PAGE_DOWN, KeyEvent.VK_NUM_LOCK,
        KeyEvent.VK_CAPS_LOCK, KeyEvent.VK_ESCAPE, KeyEvent.VK_ALT, KeyEvent.VK_ALT_GRAPH, KeyEvent.VK_CONTROL,
        KeyEvent.VK_F1, KeyEvent.VK_F2, KeyEvent.VK_F3, KeyEvent.VK_F4, KeyEvent.VK_F5, KeyEvent.VK_F6,
        KeyEvent.VK_F7, KeyEvent.VK_F8, KeyEvent.VK_F9, KeyEvent.VK_F10, KeyEvent.VK_F11, KeyEvent.VK_F12,
        KeyEvent.VK_F13, KeyEvent.VK_F14, KeyEvent.VK_F15, KeyEvent.VK_F16, KeyEvent.VK_F17, KeyEvent.VK_F18,
        KeyEvent.VK_F19, KeyEvent.VK_F20, KeyEvent.VK_F21, KeyEvent.VK_F22, KeyEvent.VK_F23, KeyEvent.VK_F24
    };

    /**
     * Creates new GeneralGUI
     */
    public GeneralGUI() {
    }

    public static void centerComponent(java.awt.Component component) {
        if (dimension == null) {
            dimension = Toolkit.getDefaultToolkit().getScreenSize();
        }
        component.setLocation(new java.awt.Point((dimension.width - component.getSize().width) / 2,
                (dimension.height - component.getSize().height) / 2));
    }

    /**
     * Sets the location of the window relative to the specified component. If
     * the component is not currently showing, or
     * <code>c</code> is
     * <code>null</code>, the window is centered on the screen. If the bottom of
     * the component is offscreen, the window is placed to the side of the
     * <code>Component</code> that is closest to the center of the screen. So if
     * the
     * <code>Component</code> is on the right part of the screen, the
     * <code>Window</code> is placed to its left, and visa versa.
     *
     * @param c the component in relation to which the window's location is
     * determined in awt.Window since 1.4 for compatibility with 1.3
     */
    public static void setLocationRelativeTo(Component component, Component c) {
        Container root = null;

        if (c != null) {
            if (c instanceof Window) {
                root = (Container) c;
            } else {
                Container parent;
                for (parent = c.getParent(); parent != null; parent = parent.getParent()) {
                    if (parent instanceof Window) {
                        root = parent;
                        break;
                    }
                }
            }
        }

        if ((c != null && !c.isShowing()) || root == null
                || !root.isShowing()) {
            Dimension paneSize = component.getSize();
            Dimension screenSize = component.getToolkit().getScreenSize();

            component.setLocation((screenSize.width - paneSize.width) / 2,
                    (screenSize.height - paneSize.height) / 2);
        } else {
            Dimension invokerSize = c.getSize();
            Point invokerScreenLocation;



            invokerScreenLocation = new Point(0, 0);
            Component tc = c;
            while (tc != null) {
                Point tcl = tc.getLocation();
                invokerScreenLocation.x += tcl.x;
                invokerScreenLocation.y += tcl.y;
                if (tc == root) {
                    break;
                }
                tc = tc.getParent();
            }



            Rectangle windowBounds = component.getBounds();
            int dx = invokerScreenLocation.x + ((invokerSize.width - windowBounds.width) >> 1);
            int dy = invokerScreenLocation.y + ((invokerSize.height - windowBounds.height) >> 1);
            Dimension ss = component.getToolkit().getScreenSize();

            if (dy + windowBounds.height > ss.height) {
                dy = ss.height - windowBounds.height;
                dx = invokerScreenLocation.x < (ss.width >> 1) ? invokerScreenLocation.x + invokerSize.width
                        : invokerScreenLocation.x - windowBounds.width;
            }
            if (dx + windowBounds.width > ss.width) {
                dx = ss.width - windowBounds.width;
            }
            if (dx < 0) {
                dx = 0;
            }
            if (dy < 0) {
                dy = 0;
            }
            component.setLocation(dx, dy);
        }
    }

    // since 1.0.1
    public static void maximizeComponent(java.awt.Component component) {
        if (dimension == null) {
            dimension = Toolkit.getDefaultToolkit().getScreenSize();
        }
        component.setSize(dimension);
        component.setLocation(0, 0);
    }

    public static boolean isLookAndFeelAvailable(String s) {
        try {
            Class myclass = Class.forName(s);
            LookAndFeel lookandfeel = (LookAndFeel) myclass.newInstance();
            return lookandfeel.isSupportedLookAndFeel();
        } catch (Exception e) {
            return false;
        }
    }

    /*
     * applyOrientation(): Borrowed from SwingApplet demo!
     */
    public static void applyOrientation(Component c, ComponentOrientation o) {
        c.setComponentOrientation(o);
        if (c instanceof JMenu) {
            JMenu menu = (JMenu) c;
            int ncomponents = menu.getMenuComponentCount();
            for (int i = 0; i < ncomponents; ++i) {
                applyOrientation(menu.getMenuComponent(i), o);
            }
        } else if (c instanceof Container) {
            Container container = (Container) c;
            int ncomponents = container.getComponentCount();
            for (int i = 0; i < ncomponents; ++i) {
                applyOrientation(container.getComponent(i), o);
            }
        }
    }

    public static void resizeComboBoxPopup(JComboBox box) {
        if (box.getSize().getWidth() == 0.0) {
            return;
        }
        FontMetrics fm = box.getFontMetrics(box.getFont());
        BasicComboPopup popup = (BasicComboPopup) box.getUI().getAccessibleChild(box, 0); // Popup
        if (popup == null) {
            return;
        }
        int size = (int) box.getPreferredSize().getWidth();
        for (int i = 0; i < box.getItemCount(); i++) {
            String str = box.getItemAt(i).toString();
            if (size < fm.stringWidth(str)) {
                size = fm.stringWidth(str);
            }
        }
        size += 40; // hotfix
        int h = ((JComponent) box.getRenderer()).getPreferredSize().height;
        int min = Math.min(box.getItemCount(), box.getMaximumRowCount());

        Component comp = popup.getComponent(0); // JScrollPane
        JScrollPane scrollpane = null;
        if (comp instanceof JScrollPane) {
            scrollpane = (JScrollPane) comp;
            if (box.getItemCount() > box.getMaximumRowCount()) {
                size += scrollpane.getVerticalScrollBar().getPreferredSize().getWidth();
            }
        }
        int max = Math.max(size, (int) box.getSize().getWidth());

        max = Math.min(max, 640);


        popup.setPreferredSize(new Dimension(max, min * h + 2)); // width, height
        popup.setLayout(new BorderLayout());
        popup.add(comp, BorderLayout.CENTER);
    }

    public static javax.swing.ComboBoxModel getComboBoxModel(Vector data) {
        javax.swing.JComboBox cb = new javax.swing.JComboBox(data);
        return cb.getModel();
    }

    
    public static javax.swing.ComboBoxModel getComboBoxModel(JLabel[] data) {
        javax.swing.JComboBox cb = new javax.swing.JComboBox(data);
        return cb.getModel();
    }

    public static javax.swing.ComboBoxModel getComboBoxModel(String[] data) {
        javax.swing.JComboBox cb = new javax.swing.JComboBox(data);
        return cb.getModel();
    }

    public static javax.swing.ListModel getListModel(Vector data) {
        javax.swing.JList jl = new javax.swing.JList(data);
        return jl.getModel();
    }
/*
    public static javax.swing.ListModel getListModel(List<?> data) {
        MyListModel myListModel = new MyListModel(data);
        return myListModel;
    }

    class MyListModel extends AbstractListModel {

        private final List<?> myList;

        public MyListModel(List<?> myList) {
            this.myList = myList;
        }

        @Override
        public int getSize() {
            return myList.size();
        }

        @Override
        public Object getElementAt(int index) {
            return myList.get(index);
        }
    }
    
    */
}
