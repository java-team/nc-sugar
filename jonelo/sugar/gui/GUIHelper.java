/**
 * ****************************************************************************
 *
 * Sugar for Java 2.0.0 Copyright (C) 2001-2011 Dipl.-Inf. (FH) Johann Nepomuk
 * Loefflmann, All Rights Reserved, http://www.jonelo.de
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author jonelo@jonelo.de
 *
 ****************************************************************************
 */
package jonelo.sugar.gui;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * GUIHelper is a helper class for the Graphical User Interface.
 */
public class GUIHelper {

    /**
     * Opens a local file in the browser.
     *
     * @param filename the local filename
     * @throws IOException if an error occurs during the opening
     * @throws URISyntaxException if the URI is invalid
     */
    public static void openLocalFileInBrowser(String filename)
            throws IOException, URISyntaxException {

        String address = makeLocalAddressOfLocalFilename(filename);
        openInBrowser(address);
    }

    public static void openLocalFileInBrowser(String filename, String browserAbsolutePath)
            throws IOException, URISyntaxException {

        String address = makeLocalAddressOfLocalFilename(filename);
        openInBrowser(address, browserAbsolutePath);
    }

    private static String makeLocalAddressOfLocalFilename(String filename)
            throws IOException, URISyntaxException {
        File file = new File(filename);
        String address = "file:///" + file.getCanonicalPath().replace('\\', '/').replaceAll(" ", "%20");
        return address;
    }
    private static final String browser = System.getProperty("browser");
    private static final String calculator = System.getProperty("calculator");

    /**
     * Opens an address in the standard browser. If the Desktop API is not
     * supported by the JRE, it will try to launch the browser that has been
     * specified by the system property called "browser".
     *
     * @param address the address to be opened
     * @throws IOException if an error occurs during the opening
     * @throws URISyntaxException if the URI is invalid
     */
    public static void openInBrowser(String address)
            throws IOException, URISyntaxException {
        Desktop desktop = null;
        if (browser == null) {
            if (Desktop.isDesktopSupported()) {
                desktop = Desktop.getDesktop();
                if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
                    URI uri = new URI(address);
                    desktop.browse(uri);
                } else {
                    openInBrowserFallback(address);
                }
            } else {
                openInBrowserFallback(address);
            }
        } else {
            openInBrowserFallback(address);
        }
    }

    private static void openInBrowserFallback(String address) {
        Logger logger = Logger.getLogger("jonelo.sugar.gui.GUIHelper");
        if (browser != null) {
            // fallback I
            // property called "browser" has been set
            logger.log(Level.CONFIG, "System property browser has been set, using {0}", browser);
            openInBrowser(address, browser);
        } else {
            logger.config("System property browser not set.");
            // fallback II
            // property called "browser" is empty or not set
            String os = System.getProperty("os.name").toLowerCase(Locale.US);
            if (os.equalsIgnoreCase("Mac OS X")) {
                // JDK 7u4 and JDK 7u5 don't support the Desktop API
                logger.log(Level.INFO, "Launching {0} using the open command.", address);
                openInBrowser(address, "open");
            }
            if (os.startsWith("linux") || os.startsWith("freebsd")) {
                logger.log(Level.INFO, "Launching {0} using the xdg-open command.", address);
                openInBrowser(address, "xdg-open");
            }
        }
    }

    // method to bypass the Desktop API
    // useful if
    // - the vendor of the JDK does not fully support the Desktop API (e.g. 
    //   the eComStation 2.0 GA: C:\PROGRAMS\Firefox\firefox!.exe
    //   or if
    // - the user want a different browser to watch the on-line help for example
    public static void openInBrowser(String address, String browserAbsolutePath) {
        List<String> command = new ArrayList<String>();
        command.add(browserAbsolutePath);
        command.add(address);

        try {
            Process process = new ProcessBuilder(command).start();
        } catch (IOException e) {
            System.err.println(e);
        }
    }

    /**
     * Opens an email in the email client, ready to send.
     *
     * @param email the email address
     * @param subject the subject of the email
     * @param body the body of the email
     * @throws IOException if an error occurs during the opening
     * @throws URISyntaxException if the URI is invalid
     */
    public static void openMail(String email, String subject, String body)
            throws IOException, URISyntaxException {

        Desktop desktop = null;
        if (Desktop.isDesktopSupported()) {
            desktop = Desktop.getDesktop();
            if (desktop != null && desktop.isSupported(Desktop.Action.MAIL)) {
                String mailTo = email + "?SUBJECT=" + subject + "&BODY=" + body;

                URI uri = new URI("mailto", mailTo, null);
                desktop.mail(uri);
            }
        }
    }

    /**
     * Opens the native calculator of the operating system. Windows, Linux,
     * Freebsd, Solaris, HP-UX, OS/2 and Mac OS X are supported.
     */
    public static void openNativeCalculator() {
        String exec = null;
        String exec_alt = null;

        if (calculator != null) {
            exec = calculator;
        } else {

            String os = System.getProperty("os.name").toLowerCase(Locale.US);

            if (os.startsWith("windows")) {
                exec = "calc.exe";
            } else if (os.startsWith("linux")
                    || os.startsWith("freebsd")
                    || os.startsWith("hp-ux")) {
                String kde = System.getProperty("user.home") + "/.kde";
                if (new File(kde).exists()) {
                    // System.err.println("Folder called "+ kde + " found, trying kcalc ...");
                    exec = "kcalc"; // KDE
                    exec_alt = "gnome-calculator"; // if KDE was installed formerly only
                    String kde4 = System.getProperty("user.home") + "/.kde4";
                    if (new File(kde4).exists()) {
                        exec = "kcalc"; // KDE
                        exec_alt = "gnome-calculator"; // if KDE was installed formerly only                    
                    }
                } else {
                    exec = "gnome-calculator"; // Gnome  or /usr/bin/gcalctool
                    exec_alt = "galculator"; // On Salix with Xfce
                }
            } else if (os.equalsIgnoreCase("Mac OS X")) {
                exec = "/Applications/Calculator.app/Contents/MacOS/Calculator";
            } else if (os.startsWith("sunos")) {
                exec = "gnome-calculator"; // Gnome
                exec_alt = "dtcalc"; // CDE            
            } else if (os.equalsIgnoreCase("OS/2")) {
                exec = "ecscalc"; // eComStation's calculator
            } else if (os.equalsIgnoreCase("AIX")) {
                exec = "dtcalc"; // CDE
                exec_alt = "gnome-calculator"; // Gnome
            } else if (os.equalsIgnoreCase("BeOS")) {
                exec = "/boot/apps/calc/calc";
            }
        }
        if (exec != null) {
            try {
                Process process = new ProcessBuilder(exec).start();
            } catch (IOException e) {
                System.err.println(e.toString());
                if (exec_alt != null) {
                    try {
                        System.out.println("Trying " + exec_alt + " ...");
                        Process process = new ProcessBuilder(exec_alt).start();
                    } catch (IOException e2) {
                        System.err.println(e2.toString());
                    }
                }
            }
        } else {
            System.err.println("No calculator has been found. Specify one with the JVM option -Dcalculator=<yourcalculator>.");
        }
    }
}
