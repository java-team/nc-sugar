/******************************************************************************
 *
 * Sugar for Java 2.0.0
 * Copyright (C) 2001-2011  Dipl.-Inf. (FH) Johann Nepomuk Loefflmann,
 * All Rights Reserved, http://www.jonelo.de
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @author jonelo@jonelo.de
 *
 *****************************************************************************/
package jonelo.sugar.math;
import java.math.BigDecimal;

public class GeneralMath {
    
    public static String decimal2Scientific(String str, int significant) {
        StringBuffer sb = new StringBuffer(str);
        boolean signflag=false;
        
        char sign=sb.charAt(0);
        if ((sign=='-') || (sign=='+')) {
            signflag=true;
            sb.deleteCharAt(0);
        }
        
        // remove leading zeros
        while ((sb.length() > 0) && (sb.charAt(0)=='0')) {
            sb.deleteCharAt(0);
        }
        if (sb.length()==0) sb.append('0');
        
        // remove dot
        // where is the decimal point?
        // or in other words:
        // what's the exponent?
        int point = sb.toString().indexOf("."); // sb.indexOf is 1.4+ only
        if (point == -1) point=sb.length();
        else sb.deleteCharAt(point);
        
        while ((sb.length() > 1) && (sb.charAt(0)=='0')) {
            sb.deleteCharAt(0);
            point--;
        }
        
        // remove tailing zeros
        while ((sb.length() > 1) && (sb.charAt(sb.length()-1)=='0')) {
            sb.deleteCharAt(sb.length()-1);
        }
        
        if (sb.length()==1 && sb.charAt(0)=='0') {
            // it's just 0, fix the point
            point=1;
        } else {
            int nks=0; // Nachkommastellen
            if (significant > 0) {
                while (sb.length() < significant) {
                    sb.append('0');
                }
                while (sb.length()-1 > significant) {
                    sb.deleteCharAt(sb.length()-1);
                }
                nks=significant-1;
            } else {
                nks=sb.length()-1;
            }
            
            BigDecimal big = new BigDecimal(sb.toString());
            big=big.movePointLeft(sb.length()-1);
            big=big.setScale(nks, BigDecimal.ROUND_HALF_UP);
            sb=new StringBuffer(big.toString());
        }
        
        // add exponent
        sb.append('e');
        sb.append(Integer.toString(point-1));
        
        // add sign
        if (signflag) sb.insert(0,sign);
        return sb.toString();
    }
    
    public static String decimal2Scientific(String str) {
        return decimal2Scientific(str,0);
    }
    
    
}
