/******************************************************************************
 *
 * Sugar for Java 2.0.0
 * Copyright (C) 2001-2015  Dipl.-Inf. (FH) Johann Nepomuk Loefflmann,
 * All Rights Reserved, http://www.jonelo.de
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @author jonelo@jonelo.de
 *
 *****************************************************************************/
package jonelo.sugar.math;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A Rational.
 */
public class Rational {

    private BigInteger zaehler;
    private BigInteger nenner;
    final static String POSINT = "(\\d+)";
    final static String BLANK = " ";

    public Rational() {
        zaehler = BigInteger.ZERO;
        nenner = BigInteger.ONE;
    }

    public Rational(BigInteger zaehler) {
        this.zaehler = zaehler;
        this.nenner = BigInteger.ONE;
    }

    public Rational(int zaehler, int nenner) {
        this((long) zaehler, (long) nenner);
    }
    
    public Rational(int zaehler) {
        this((long)zaehler, 1L);
    }
    
    public Rational(long zaehler) {
        this(zaehler, 1L);
    }

    public Rational(long zaehler, long nenner) {
        this.zaehler = BigInteger.valueOf(zaehler);
        this.nenner = BigInteger.valueOf(nenner);
    }

    public Rational(BigInteger zaehler, BigInteger nenner) {
        if (nenner.equals(BigInteger.ZERO)) {
            throw new ArithmeticException("division by 0");
        }
        this.zaehler = zaehler;
        this.nenner = nenner;
        kuerzen();
    }

    public Rational(BigDecimal decimal) {
        int signum = decimal.signum();

        BigDecimal ganze = BigDecimal.ZERO;
        BigDecimal rest = decimal;

        if (decimal.compareTo(BigDecimal.ONE) >= 0) {
            ganze = decimal.setScale(0, RoundingMode.DOWN);
            rest = decimal.subtract(ganze);
        }
        int scale = rest.scale();
        zaehler = rest.abs().movePointRight(scale).toBigInteger();
        nenner = BigDecimal.ONE.movePointRight(scale).toBigInteger();
        zaehler = ganze.toBigIntegerExact().multiply(nenner).add(zaehler).multiply(BigInteger.valueOf(signum));

        kuerzen();

    }

    public void signBeautyfuling() {
        int signum = zaehler.signum() * nenner.signum();
        zaehler = zaehler.abs();
        nenner = nenner.abs();
        if (signum == -1) {
            zaehler = zaehler.multiply(BigInteger.valueOf(signum));
        }
    }

    private boolean checkNormalInput(String input) {
        final Pattern pattern1 = Pattern.compile("^\\s*([-]??)\\s*(\\d+)\\s*/\\s*(\\d+)\\s*$");
        Matcher matcher = pattern1.matcher(input);
        boolean negative = false;
        if (matcher.find()) {
            negative = matcher.group(1).equals("-");
            zaehler = new BigInteger(matcher.group(2));
            nenner = new BigInteger(matcher.group(3));
            if (nenner.equals(BigInteger.ZERO)) {
                throw new ArithmeticException("division by 0");
            }
            if (negative) {
                zaehler = zaehler.multiply(BigInteger.valueOf(-1));
            }
//            signBeautyfuling();
            return true;
        } else {
            return false;
        }
    }

    private boolean checkEmptyInput(String input) {
        if (input.length() == 0) {
            zaehler = BigInteger.ZERO;
            nenner = BigInteger.ONE;
            return true;
        } else {
            return false;
        }
    }

    private boolean checkMixedInput(String input) {
        final String INT = "([-]??\\d+)";
        final Pattern pattern2 = Pattern.compile("^\\s*" + INT + BLANK + "\\s*" + POSINT + "\\s*/\\s*" + POSINT + "\\s*$");
        Matcher matcher = pattern2.matcher(input);
        if (matcher.find()) {
            BigInteger ganze = new BigInteger(matcher.group(1));
            int signum = ganze.signum();
            ganze = ganze.abs();
            zaehler = new BigInteger(matcher.group(2));
            nenner = new BigInteger(matcher.group(3));
            if (nenner.equals(BigInteger.ZERO)) {
                throw new ArithmeticException("division by 0");
            }
            zaehler = ganze.multiply(nenner).add(zaehler);
            if (signum == -1) {
                zaehler = zaehler.multiply(BigInteger.valueOf(signum));
            }
            return true;
        } else {
            return false;
        }
    }

    private boolean checkDecimalInput(String input) {
        final String periodDelimiters = "[\\~_pP\u00AF]{1}";
        final Pattern patternExact = Pattern.compile("^([-]??\\d+)(\\.\\d*(" + periodDelimiters + "\\d+)??)??$");

        Matcher matcher = patternExact.matcher(input);

        if (matcher.find()) {
            Rational result = new Rational();
            boolean negative = false;

            // an integer
            Pattern patternInteger = Pattern.compile("^([-]??\\d+)$");
            matcher = patternInteger.matcher(input);
            if (matcher.find()) {
                zaehler = new BigInteger(input);
                nenner = BigInteger.ONE;
                return true;
            }

            // a decimal number, get both parts
            String afterTheComma = null;
            Pattern patternDecimal = Pattern.compile("^([-]??)(\\d+)\\.(.+)$");
            matcher = patternDecimal.matcher(input);
            if (matcher.find()) {
                negative = matcher.group(1).equals("-");
                result.setZaehler(new BigInteger(matcher.group(2)).abs());
                result.setNenner(BigInteger.ONE);

                afterTheComma = matcher.group(3);
            }

            // after the comma
            Pattern patternNoPeriod = Pattern.compile("^(\\d+)$");
            matcher = patternNoPeriod.matcher(afterTheComma);
            if (matcher.find()) {
                String nonPeriodicPart = matcher.group(1);
                result = result.add(nonPeriodicRational(nonPeriodicPart));
            }

            Pattern patternPeriod = Pattern.compile("^(\\d*)(" + periodDelimiters + ")(\\d+)$");
            matcher = patternPeriod.matcher(afterTheComma);
            if (matcher.find()) {
                String nonPeriodicPart = matcher.group(1);
                if (nonPeriodicPart.length() > 0) {
                    result = result.add(nonPeriodicRational(nonPeriodicPart));
                }

                String periodicPart = matcher.group(3);
                result = result.add(periodicRational(nonPeriodicPart, periodicPart));
            }

            nenner = result.getNenner();
            if (nenner.equals(BigInteger.ZERO)) {
                throw new ArithmeticException("division by 0");
            }

            zaehler = result.getZaehler();
            if (negative) {
                zaehler = zaehler.multiply(BigInteger.valueOf(-1));
            }

            return true;
        } else {
            return false;
        }
    }

    private Rational periodicRational(String nonPeriodicPart, String periodicPart) {
        BigInteger z2 = new BigInteger(periodicPart);
        BigInteger n2 =
                BigDecimal.ONE.movePointRight(periodicPart.length()).subtract(BigDecimal.ONE).movePointRight(nonPeriodicPart.length()).toBigInteger();
        System.out.println(z2 + "/" + n2);
        return new Rational(z2, n2);
    }

    private Rational nonPeriodicRational(String nonPeriodicPart) {
        BigInteger z = new BigInteger(nonPeriodicPart);
        BigInteger n = BigDecimal.ONE.movePointRight(nonPeriodicPart.length()).toBigInteger();
        return new Rational(z, n);
    }
    public final static int EMPTY = 0, NORMAL = 1, MIXED = 2, DECIMAL = 3;

    public Rational(String input, int type) {
        boolean success = false;
        switch (type) {
            case EMPTY:
                success = checkEmptyInput(input);
                break;
            case MIXED:
                success = checkMixedInput(input);
                break;
            case DECIMAL:
                success = checkDecimalInput(input);
                break;
            case NORMAL:
            default:
                success = checkNormalInput(input);
                break;
        }
        if (!success) {
            throw new IllegalArgumentException("invalid");
        }
        kuerzen();
    }

    // takes strings in the format
    // 3 1/2  or 4/8  or 2.1234 or 2.123~456
    public Rational(String input) {
        boolean validInput = false;
        validInput = checkEmptyInput(input);
        if (!validInput) {
            validInput = checkNormalInput(input);
        }
        if (!validInput) {
            validInput = checkMixedInput(input);
        }
        if (!validInput) {
            validInput = checkDecimalInput(input);
        }
        if (!validInput) {
            throw new IllegalArgumentException("invalid");
        }
        kuerzen();
    }

    public BigInteger getZaehler() {
        return zaehler;
    }

    public BigInteger getNenner() {
        return nenner;
    }

    public boolean isNegative() {
        int signum = zaehler.signum() * nenner.signum();
        return signum == -1;
    }

    public Rational add(Rational rational) {
        Rational ret = new Rational();
        Rational temp = rational;
        temp.signBeautyfuling();
        signBeautyfuling();
        ret.setZaehler((getZaehler().multiply(rational.getNenner())).add(getNenner().multiply(rational.getZaehler())));
        ret.setNenner(getNenner().multiply(rational.getNenner()));  // Hauptnenner
        ret.signBeautyfuling();
        ret.kuerzen();
        return ret;
    }

    public Rational multiply(Rational rational) {
        Rational ret = new Rational();
        Rational temp = rational;
        temp.signBeautyfuling();
        signBeautyfuling();
        ret.setZaehler(getZaehler().multiply(rational.getZaehler()));
        ret.setNenner(getNenner().multiply(rational.getNenner()));
        ret.signBeautyfuling();
        ret.kuerzen();
        return ret;
    }

    /**
     * @param zaehler the zaehler to set
     */
    public void setZaehler(BigInteger zaehler) {
        this.zaehler = zaehler;
    }

    /**
     * @param nenner the nenner to set
     */
    public void setNenner(BigInteger nenner) {
        this.nenner = nenner;
    }

    private void kuerzen() {
        BigInteger gcd = zaehler.gcd(nenner);
        this.zaehler = zaehler.divide(gcd);
        this.nenner = nenner.divide(gcd);
    }

    public String toDecStringRounded(int scale) {
        BigDecimal ret = new BigDecimal(zaehler).divide(new BigDecimal(nenner), scale, BigDecimal.ROUND_HALF_UP);
        return ret.toString();
    }

    public String toSciStringRounded(int precision) {
        BigDecimal result = new BigDecimal(zaehler).divide(new BigDecimal(nenner), precision, BigDecimal.ROUND_HALF_UP);
        return GeneralMath.decimal2Scientific(result.toPlainString(), precision);
    }

    public String toDecStringExact() {
        StringBuilder sb = new StringBuilder();
        BigInteger value[] = zaehler.abs().divideAndRemainder(nenner);
        if (zaehler.compareTo(BigInteger.ZERO) < 0) {
            sb.append("-");
        }
        sb.append(value[0]);
        if (value[1].compareTo(BigInteger.ZERO) > 0) {
            sb.append(asComma(value[1], nenner));
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        return zaehler + "/" + nenner;
    }

    // 25/6 = 4 1/6
    public String toMixedString() {
        StringBuilder sb = new StringBuilder();
        BigInteger value[] = zaehler.abs().divideAndRemainder(nenner);
        if (zaehler.compareTo(BigInteger.ZERO) < 0) { // negative
            sb.append("-");
        }
        if (value[0].compareTo(BigInteger.ZERO) > 0) { // rest
            sb.append(value[0]);
            sb.append(" ");
        }
        sb.append(value[1]);
        sb.append("/");
        sb.append(nenner);
        return sb.toString();
    }

    /**
     * Returns a String whose value represents exactly dividend / divisor
     * A period is marked by a ~
     * @param dividend The dividend
     * @param divisor The divisor
     * @return a String whose value represents exactly dividend / divisor
     */
    public static String asComma(BigInteger dividend, BigInteger divisor) {
        final BigInteger TEN = BigInteger.valueOf(10);
        StringBuilder sb = new StringBuilder();

        BigInteger ganz = BigInteger.ZERO;

        // es k�nnen immer nur h�chstens divisor-1 verschiedene Reste auftreten
        BigInteger rest = BigInteger.valueOf(-1);
        ArrayList<BigInteger> arrayList = new ArrayList<BigInteger>();
        boolean ende = false;

        for (int i = 0; (BigInteger.valueOf(i)).compareTo(divisor) < 0; i++) {
            if (rest.equals(BigInteger.ZERO)) {
                break;
            }
            // Wenn Rest 0 vorkommt, ist die Division zu Ende und der Dezimalbruch endlich
            dividend = dividend.multiply(TEN);

            // ist dividend schon mal vorgekommen? => break, periode!
            for (int k = 0; k < arrayList.size(); k++) {
                if (((BigInteger) arrayList.get(k)).equals(dividend)) {
                    sb.insert(k, "\u00AF");
                    ende = true;
                    break;
                }
            }

            if (ende) {
                break;
            }

            // dividend merken
            arrayList.add(dividend);
            while (dividend.compareTo(divisor) < 0) {
                sb.append("0");
                dividend = dividend.multiply(TEN);
                arrayList.add(dividend);
            }
            ganz = dividend.divide(divisor);
            sb.append(ganz.toString());
            rest = dividend.mod(divisor);
            dividend = rest;
        }
        sb.insert(0, ".");
        return sb.toString();
    }
}
