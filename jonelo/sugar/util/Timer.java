/******************************************************************************
 *
 * Sugar for Java 2.0.0
 * Copyright (C) 2001-2006  Dipl.-Inf. (FH) Johann Nepomuk Loefflmann,
 * All Rights Reserved, http://www.jonelo.de
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @author jonelo@jonelo.de
 *
 *****************************************************************************/

package jonelo.sugar.util;

public class Timer {
    private long begin, end;
    
    /** Creates new Timer */
    public Timer() {
    }
    
    public void start() {
        begin = System.currentTimeMillis();
    }
    
    public void stop() {
        end = System.currentTimeMillis();
    }
    
    public String toString() {
        return duration(end-begin);
    }
    
    public long getAsMillis() {
        return (end-begin);
    }
    
    public long getAsSeconds() {
        return ((end-begin) / 1000);
    }
    
    private static final String duration(long t) {
        long ms=0,s=0,m=0,h=0,d=0;
        ms = t % 1000; t /= 1000;
        if (t > 0) {
            s  = t % 60; t /= 60; }
        if (t > 0) {
            m = t % 60; t /= 60; }
        if (t > 0) {
            h = t % 24; t /= 60; }
        d = t;
        
        return (d+" d, "+
                h+" h, "+
                m+" m, "+
                s+" s, "+
                ms+" ms");
    }
}
