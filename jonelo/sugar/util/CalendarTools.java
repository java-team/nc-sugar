/*
 * CalendarTools.java
 *
 * Created on 26. Juli 2007, 21:44
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package jonelo.sugar.util;

import java.util.Calendar;

/**
 *
 * @author jonelo
 */
public class CalendarTools {
    
    private static final int[] daysInMonth = { // Jan-Dec
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    };
    
    /** Creates a new instance of CalendarTools */
    public CalendarTools() {
    }

    public static boolean isLeapYear(int year) {
        return (year % 400 == 0) || ((year % 100 != 0) && (year % 4 == 0));
    }
    
    public static int daysInMonth(int year, int month) {
        int result = daysInMonth[month];
        if ((month == Calendar.FEBRUARY) && (isLeapYear(year))) {
            result++;
        }
        return result;
    }
}
