/******************************************************************************
 *
 * Sugar for Java 2.0.0
 * Copyright (C) 2001-2006  Dipl.-Inf. (FH) Johann Nepomuk Loefflmann,
 * All Rights Reserved, http://www.jonelo.de
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @author jonelo@jonelo.de
 *
 *****************************************************************************/
package jonelo.sugar.util;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class ExtendedGregorianCalendar extends java.util.GregorianCalendar {

    public static final int LAST = -1,  FIRST = 1,  SECOND = 2,  THIRD = 3,  FOURTH = 4;
    /**
     * 
     */
    public static String ERROR_1583 = "Year must be 1583 or later";

    /** Creates a new instance of ExtendedGregorianCalendar */
    public ExtendedGregorianCalendar() {
        super();
    }

    public ExtendedGregorianCalendar(TimeZone timezone) {
        super(timezone);
    }
    
    public ExtendedGregorianCalendar(GregorianCalendar gc) {
        setTimeInMillis(gc.getTimeInMillis());
        setTimeZone(gc.getTimeZone());
    }
    
   public final static int[][] nearestWeekdayMatrix = {
       {  0,  1,  2,  3, -3, -2, -1 },
       { -1,  0,  1,  2,  3, -3, -2 },
       { -2, -1,  0,  1,  2,  3, -3 },
       { -3, -2, -1,  0,  1,  2,  3 },
       {  3, -3, -2, -1,  0,  1,  2 },
       {  2,  3, -3, -2, -1,  0,  1 },
       {  1,  2,  3, -3, -2, -1,  0 }
   };


   public final static int[][] nextWeekdayMatrix = {
       { 7, 1, 2, 3, 4, 5, 6 },
       { 6, 7, 1, 2, 3, 4, 5 },
       { 5, 6, 7, 1, 2, 3, 4 },
       { 4, 5, 6, 7, 1, 2, 3 },
       { 3, 4, 5, 6, 7, 1, 2 },
       { 2, 3, 4, 5, 6, 7, 1 },
       { 1, 2, 3, 4, 5, 6, 7 }
   };

   private final static int[][] previousWeekdayMatrix = {
       { -7, -6, -5, -4, -3, -2, -1 },
       { -1, -7, -6, -5, -4, -3, -2 },
       { -2, -1, -7, -6, -5, -4, -3 },
       { -3, -2, -1, -7, -6, -5, -4 },
       { -4, -3, -2, -1, -7, -6, -5 },
       { -5, -4, -3, -2, -1, -7, -6 },
       { -6, -5, -4, -3, -2, -1, -7 }
   };

   // don't rely on the values of the constants, only rely on the constants
   public static int weekday2index(int weekday) {
       switch (weekday) {
          case Calendar.MONDAY:    return 0;
          case Calendar.TUESDAY:   return 1;
          case Calendar.WEDNESDAY: return 2;
          case Calendar.THURSDAY:  return 3;
          case Calendar.FRIDAY:    return 4;
          case Calendar.SATURDAY:  return 5;
          case Calendar.SUNDAY:    return 6;
          default: throw new IllegalArgumentException("weekday value "+weekday+" is invalid");
       }
   }
   
   public void setNextWeekday(int weekday) {
       setNextWeekday(weekday, true);
   }
   
   public void setNextWeekday(int weekday, boolean moveDefinitely) {
       add(Calendar.DATE, nextWeekday(get(Calendar.DAY_OF_WEEK), weekday, moveDefinitely));
   }  

   // moveDefinetely means we move in any case
   private static int nextWeekday(int weekdayFrom, int weekdayTo, boolean moveDefinitely) {
       return getXWay(nextWeekdayMatrix, weekdayFrom, weekdayTo, moveDefinitely);
   }
   
   
   public void setPreviousWeekday(int weekday) {
       setPreviousWeekday(weekday, true);
   }

   public void setPreviousWeekday(int weekday, boolean moveDefinetely) {
       add(Calendar.DATE, previousWeekday(get(Calendar.DAY_OF_WEEK), weekday, moveDefinetely));
   }
   
   private static int previousWeekday(int weekdayFrom, int weekdayTo, boolean moveDefinitely) {
       return getXWay(previousWeekdayMatrix, weekdayFrom, weekdayTo, moveDefinitely);
   }

   
   public void setNearestWeekday(int weekday) {
       add(Calendar.DATE, nearestWeekday(get(Calendar.DAY_OF_WEEK), weekday));
   }
   
   private static int nearestWeekday(int weekdayFrom, int weekdayTo) {
       return getXWay(nearestWeekdayMatrix, weekdayFrom, weekdayTo, false);
   }

   
   private static int getXWay(int[][] lookuptable, int weekdayFrom, int weekdayTo, boolean moveDefinitely) {
       if ((weekdayFrom == weekdayTo) && !moveDefinitely) return 0;
       return lookuptable[weekday2index(weekdayFrom)][weekday2index(weekdayTo)];
   } 

   
  
   /*
     a in Julian years from 2000
        vernal equinox  365.242 374 04 + 0.000 000 103 38�a days
     northern solstice  365.241 626 03 + 0.000 000 006 50�a days
        autumn equinox  365.242 017 67 ? 0.000 000 231 50�a days
     southern solstice  365.242 740 49 ? 0.000 000 124 46�a days
    */
   private int adjustment(int year, double value, double delta) {
       if (year==2000) return 0;
       double a = (double)(year - 2000);
       double days = (value) + (delta * a);
       int seconds = (int)(days * 86400.0);
       if (year < 2000) seconds *= -1;
       return seconds;
   }

   public final static int EQUINOX_MARCH = Calendar.MARCH,
       EQUINOX_SEPTEMBER = Calendar.SEPTEMBER,
       SOLSTICE_JUNE = Calendar.JUNE,
       SOLSTICE_DECEMBER = Calendar.DECEMBER;
   
   public void setEquinox(int type, int year) throws IllegalArgumentException {
       switch (type) {
           case EQUINOX_MARCH: 
                setEquinoxSolstice(year, Calendar.MARCH, 20, 7, 26, 0, 365.24237404, 0.00000010338);
                break;
           case EQUINOX_SEPTEMBER:
                setEquinoxSolstice(year, Calendar.SEPTEMBER, 22, 17, 12, 0, 365.24201767, 0.00000023150);
                break;           
           default:
               throw new IllegalArgumentException("EQUINOX_MARCH or EQUINOX_SEPTEMBER expected.");
       }
   }

   public void setSolstice(int type, int year) throws IllegalArgumentException {
        switch(type) {
            case SOLSTICE_JUNE:
                setEquinoxSolstice(year, Calendar.JUNE, 21, 1, 37, 0, 365.24162603, 0.00000000650);
                break;
            case SOLSTICE_DECEMBER:
                setEquinoxSolstice(year, Calendar.DECEMBER, 21, 13, 25, 0, 365.24274049, 0.00000012446);
                break;
            default:
                throw new IllegalArgumentException("SOLSTICE_JUNE or SOLSTICE_DECEMBER expected.");
        }
   }
   
   private final static int J2000 = 2000;

   
   private void setEquinoxSolstice(int year, int month, int date, int hour_of_day, int minute, int second, double value, double delta) {
       // startyear
       ExtendedGregorianCalendar utccal = new ExtendedGregorianCalendar();
       
       utccal.setTimeZone(TimeZone.getTimeZone("UTC"));
       utccal.set(Calendar.YEAR, J2000);
       utccal.set(Calendar.MONTH, month);
       utccal.set(Calendar.DATE, date);
       utccal.set(Calendar.HOUR_OF_DAY, hour_of_day);
       utccal.set(Calendar.MINUTE, minute);
       utccal.set(Calendar.SECOND, second);
       int adjustment = adjustment(year, value, delta);
       for (int i=J2000; i < year; i++) {
          utccal.add(GregorianCalendar.SECOND, adjustment);
       }
       for (int i=J2000; i > year; i--) {
          utccal.add(GregorianCalendar.SECOND, adjustment);
       }
       setTimeInMillis(utccal.getTimeInMillis());
   }
 
   
   /*
   vernal equinox 	365.242 374 04 + 0.000 000 103 38�a days
northern solstice 	365.241 626 03 + 0.000 000 006 50�a days
autumn equinox 	365.242 017 67 ? 0.000 000 231 50�a days
southern solstice 	365.242 740 49 ? 0.000 000 124 46�a days
   */
   
    /**
     * Meeus/Jones/Butcher Gregorian algorithm
     * This algorithm for calculating the date of Easter Sunday is
     * given by Jean Meeus in his book Astronomical Algorithms (1991),
     * which in turn cites Spencer Jones in his book General Astronomy (1922)
     * and also the Journal of the British Astronomical Association (1977).
     * This algorithm also appears in The Old Farmer's Almanac (1977), p. 69.
     * The JBAA cites Butcher's Ecclesiastical Calendar (1876).
     * The method is valid for all Gregorian years and has no exceptions
     * and requires no tables. 
     *
     *
     * From chapter 4 of "Astronomical Formulae for Calculators" 2nd
     * edition; by Jean Meeus; publisher: Willmann-Bell Inc.,
     * ISBN 0-943396-01-8 ...
     *
     *                          Date of Easter.
     *
     * The method used below has been given by Spencer Jones in his
     * book "General Astronomy" (pages 73-74 of the edition of 1922).
     * It has been published again in the "Journal of the British
     * Astronomical Association", Vol.88, page 91 (December 1977)
     * where it is said that it was devised in 1876 and appeared in
     * the Butcher's "Ecclesiastical Calendar."
     *
     * Unlike the formula given by Guass, this method has no exception
     * and is valid for all years in the Gregorian calendar, that is
     * from the year 1583 on.
     *
     * [...text omitted...]
     *
     * The extreme dates of Easter are March 22 (as in 1818 and 2285)
     * and April 25 (as in 1886, 1943, 2038).
     * ===============================================================
     *
     * The following Modula-2 code by Greg Vigneault, April 1993.
     * Converted to Pascal by Kerry Sokalsky
     * Converted to Java by Johann N. Loefflmann
     */
    /**
     * setzt den Ostertag des �bergebenen Jahres
     * @param year das Jahr in dessen der Ostertag zu bestimmen ist.
     * @throws java.lang.Exception 
     */
    public void setEaster(int year) throws Exception {
        int a, b, c,
                d, e, f,
                g, h, i,
                k, l, m,
                n, p;

        if ((year < 1583) || (year < 0)) {
            throw new Exception(ERROR_1583);
        }

        a = year % 19;
        b = year / 100;
        c = year % 100;
        d = b / 4;
        e = b % 4;
        f = (b + 8) / 25;
        g = (b - f + 1) / 3;
        h = (19 * a + b - d - g + 15) % 30;
        i = c / 4;
        k = c % 4;
        l = (32 + 2 * e + 2 * i - h - k) % 7;
        m = (a + 11 * h + 22 * l) / 451;
        p = (h + l - 7 * m + 114);
        n = p / 31;                  // n = month number 3 or 4
        p = (p % 31) + 1;            // p = day in month

        set(Calendar.DATE, p);
        set(Calendar.MONTH, n - 1);
        set(Calendar.YEAR, year);
        setTime(getTime());
    }

    /**
     * http://en.wikipedia.org/wiki/Computus#Meeus_Julian_algorithm
     * Meeus Julian algorithm
     * Jean Meeus, in his book Astronomical Algorithms (1991) presents
     * the following formula for calculating Easter Sunday in Julian years.
     * The method is valid for all Julian years and has no exceptions and
     * requires no tables.
     *
     * returns the Easter Day (Julian calendar)
     * This is not the Gregorian Easter now used by Western churches!
     */
    private void setEasterJulian(int year) throws Exception {
        int a, b, c, d, e, p, month, day;
        a = year % 4;
        b = year % 7;
        c = year % 19;
        d = (19 * c + 15) % 30;
        e = (2 * a + 4 * b - d + 34) % 7;
        p = (d + e + 114);
        month = p / 31;   // month number
        day = (p % 31) + 1; // day in month

        set(Calendar.DATE, day);
        set(Calendar.MONTH, month - 1);
        set(Calendar.YEAR, year);
        setTime(getTime());
    }

    /**
     * set the Easter day
     * @param year 
     * @param julian if true, the julian calendar will be used (orthodox), if
     * false the western (katholic, protestantic) calendar will be used
     * @throws java.lang.Exception
     */
    public void setEaster(int year, boolean julian) throws Exception {
        if (julian) {
            setEasterJulian(year);
        } else {
            setEaster(year);
        }
    }

    // LAST, FIRST, SECOND...
    public void setNthDow(int n, int dow) {
        set(DAY_OF_WEEK, dow);
        set(DAY_OF_WEEK_IN_MONTH, n);
    // setTime(getTime());
    }

    public void setNthDow(int n, int dow, int month) {
        set(Calendar.MONTH, month);
        setNthDow(n, dow);
    }

    public void setNthDow(int n, int dow, int month, int year) {
        set(Calendar.YEAR, year);
        setNthDow(n, dow, month);
    }

    /**
     * setzt den vorhergehenden Wochentag dow
     * (wenn Tag im Objekt bereits dow ist, wird nichts gemacht)
     * @param dow the day of the week
     */
    public void setPrevDow(int dow) {
        while (get(Calendar.DAY_OF_WEEK) != dow) {
            add(Calendar.DATE, -1);
//      setTime(getTime());
        }
    }

    /**
     * setzt den nachfolgenden Wochentag dow
     * (wenn Tag im Objekt bereits dow ist, wird nichts gemacht)
     * @param dow the day of the week
     */
    public void setNextDow(int dow) {
        while (get(Calendar.DAY_OF_WEEK) != dow) {
            add(Calendar.DATE, +1);
//      setTime(getTime());
        }
    }

    // setzt den 1. Adventssonntag
    public void setAdvent(int year) {
        // Referenzdatum setzen
        set(year, Calendar.DECEMBER, 24);

        // 1. Sonntag vor dem Referenzdatum suchen
        setPrevDow(SUNDAY);

        // 3 weitere Wochen (21 Tage) zur�ckgehen
        // (insgesamt ist es dann die 4. Woche vor Weihnachten)
        add(Calendar.DATE, -21);
    // setTime(getTime());
    }

    /**
     * http://www.jguru.com/forums/view.jsp?EID=489372
     * Calculates the number of days between two calendar days in a manner
     * which is independent of the Calendar type used.
     *
     * @param d1    The first date.
     * @param d2    The second date.
     *
     * @return      The number of days between the two dates.  Zero is
     *              returned if the dates are the same, one if the dates are
     *              adjacent, etc.  The order of the dates
     *              does not matter.
     * the value returned is always >= 0.
     *              If Calendar types of d1 and d2
     *              are different, the result may not be accurate.
     */
    public static int getDaysBetween(java.util.Calendar d1, java.util.Calendar d2) {
        int factor = 1;
        if (d1.after(d2)) {  // swap dates so that d1 is start and d2 is end
            java.util.Calendar swap = d1;
            d1 = d2;
            d2 = swap;
            factor = -1;
        }
        int days = d2.get(java.util.Calendar.DAY_OF_YEAR) -
                d1.get(java.util.Calendar.DAY_OF_YEAR);
        int y2 = d2.get(java.util.Calendar.YEAR);
        if (d1.get(java.util.Calendar.YEAR) != y2) {
            d1 = (java.util.Calendar) d1.clone();
            do {
                days += d1.getActualMaximum(java.util.Calendar.DAY_OF_YEAR);
                d1.add(java.util.Calendar.YEAR, 1);
            } while (d1.get(java.util.Calendar.YEAR) != y2);
        }
        return factor * days;
    } // getDaysBetween()

    /*
    http://emr.cs.iit.edu/home/reingold/calendar-book/Calendrica.html
    1961-1969, 2018-2036
    http://www.math.nus.edu.sg/aslaksen/calendar/chinese.html
    1980: Feb 16 1999: Feb 16
    1981: Feb 5  2000: Feb 5
    1982: Jan 25 2001: Jan 24
    1983: Feb 13 2002: Feb 12
    1984: Feb 2  2003: Feb 1
    1985: Feb 20 2004: Jan 22
    1986: Feb 9  2005: Feb 9
    1987: Jan 29 2006: Jan 29
    1988: Feb 17 2007: Feb 18
    1989: Feb 6  2008: Feb 7
    1990: Jan 27 2009: Jan 26
    1991: Feb 15 2010: Feb 14
    1992: Feb 4  2011: Feb 3
    1993: Jan 23 2012: Jan 23
    1994: Feb 10 2013: Feb 10
    1995: Jan 31 2014: Jan 31
    1996: Feb 19 2015: Feb 19
    1997: Feb 7  2016: Feb 8
    1998: Jan 28 2017: Jan 28
     */
    // set the Chinese New Year date from 1900 to 2100
    public void setChineseNewYear(int year) throws Exception {
        // Chinese New Year will always fall between January 21 and February 21
        // Keep it simple, let's use simple offsets (base is Jan 31)
        // rather than complex math (and it's faster ;)

        final int firstYearKnown = 1900;
        final byte[] chineseYearDayOffset = {
            // 1885-1903
            /* 1900 */0, 19, 8, -2, // testen
            // 1904-1922
            16, 4, -6, 13, 2, -9, 10, -1, 18, 6, -5, 14, 3, -8, 11, 1, 20, 8, -3, // testen
            // 1923-1941
            16, 5, -7, 13, 2, -8, 10, -1, 17, 6, -5, 14, 4, -7, 11, 0, 19, 8, -4, // testen
            // 1942-1960
            15, 5, -6, 13, 2, -9, 10, -2, 17, 6, -4, 14, 3, -7, 12, 0, 18, 8, -3,
            // 1961-1979
            15, 5, -6, 13, 2, -10, 9, -1, 17, 6, -4, 15, 3, -8, 11, 0, 18, 7, -3,
            // 1980-1998
            16, 5, -6, 13, 2, 20, 9, -2, 17, 6, -4, 15, 4, -8, 10, 0, 19, 7, -3,
            // 1999-2017
            16, 5, -7, 12, 1, -9, 9, -2, 18, 7, -5, 14, 3, -8, 10, 0, 19, 8, -3,
            // 2018-2036
            16, 5, -6, 12, 1, -9, 10, -2, 17, 6, -5, 13, 3, -8, 11, 0, 19, 8, -3,
            // 2037-2055
            15, 4, -7, 12, 1, -9, 10, -1, 17, 6, -5, 14, 2, -8, 11, 1, 19, 8, -3, // testen
            // 2056-2074
            15, 4, -7, 12, 2, -10, 9, -2, 17, 5, -5, 14, 3, -8, 11, 0, 19, 7, -4, // testen
            // 2075-2093
            15, 5, -7, 12, 2, -9, 9, -2, 17, 6, -5, 14, 3, -7, 10, -1, 18, 7, -4,// testen
            // 2094-2112
            15, 5, -6, 12, 1, -10, 9 /* 2100 */

        };

        // internal check
        // int value = (int)chineseYearDayOffset[year-firstYearKnown];
        // if (value < -10 || value > 21) throw new Exception("internal error");

        if (year < firstYearKnown || year - firstYearKnown > chineseYearDayOffset.length - 1) {
            throw new Exception("unknown");
        }

        set(year, Calendar.JANUARY, 31);
        add(Calendar.DATE, (int) chineseYearDayOffset[year - firstYearKnown]);
    }

    public void setDSTstart(int year, TimeZone tz) throws Exception {
        TimeZone tzBackup = getTimeZone();
        setTimeZone(tz);
        // let's start with Jan 1st, and increase the date until the DST start is found
        // set hour:min:sec to 23:59:59 - this is the latest possible time for a DST switch
        set(year, Calendar.JANUARY, 1, 23, 59, 59);

        // huh, we are already in DST, let's move to non-DST first
        if (tz.inDaylightTime(getTime())) {
            while (tz.inDaylightTime(getTime()) && get(Calendar.YEAR) == year) {
                add(Calendar.DATE, 1);
            }
        }

        // find the DST start
        while (!tz.inDaylightTime(getTime()) && get(Calendar.YEAR) == year) {
            add(Calendar.DATE, 1);
        }

        // we don't have any DST in the year
        if (get(Calendar.DATE) == 1 && get(Calendar.MONTH) == Calendar.JANUARY) {
            throw new Exception("not defined");
        }

        int y = get(Calendar.YEAR);
        int m = get(Calendar.MONTH);
        int d = get(Calendar.DATE);
        setTimeZone(tzBackup);
        set(y, m, d, 0, 0, 0);


    /*
    gc.set(year, Calendar.JANUARY, 1);
    // Monat suchen, wo DST beginnt
    while (!tz.inDaylightTime(gc.getTime()) && gc.get(Calendar.YEAR)==year) {
    gc.add(Calendar.MONTH, 1);
    }
    // einen Monat zur�ck
    gc.add(Calendar.MONTH, -1);
    // Tag suchen, wo DST beginnt
    while (!tz.inDaylightTime(gc.getTime())) {
    gc.add(Calendar.DATE, 1);
    }
     */
    }

    public void setDSTend(int year, TimeZone tz) throws Exception {
        TimeZone tzBackup = getTimeZone();
        setTimeZone(tz);
        // let's start with Jan 1st, and increase the date until the DST end is found
        // set hour:min:sec to 23:59:59 - this is the latest possible time for a DST switch
        set(year, Calendar.JANUARY, 1, 23, 59, 59);

        // huh, we are already in non-DST, let's move to DST first
        if (!tz.inDaylightTime(getTime())) {
            while (!tz.inDaylightTime(getTime()) && get(Calendar.YEAR) == year) {
                add(Calendar.DATE, 1);
            }
        }

        // find the DST end
        while (tz.inDaylightTime(getTime()) && get(Calendar.YEAR) == year) {
            add(Calendar.DATE, 1);
        }

        // we don't have any DST in the year
        if (get(Calendar.DATE) == 1 && get(Calendar.MONTH) == Calendar.JANUARY) {
            throw new Exception("not defined");
        }

        int y = get(Calendar.YEAR);
        int m = get(Calendar.MONTH);
        int d = get(Calendar.DATE);
        setTimeZone(tzBackup);
        set(y, m, d, 0, 0, 0);
    /*
    if (!tz.useDaylightTime()) throw new Exception(NOT_DEFINED);
    // DST begin setzen
    setDSTbegin(gc, year, tz);
    // Monat suchen, wo DST aufh�rt
    while (tz.inDaylightTime(gc.getTime())) {
    gc.add(Calendar.MONTH, 1);
    }
    // einen Monat zur�ck
    gc.add(Calendar.MONTH, -1);
    // Tag suchen, wo DST aufh�rt
    while (tz.inDaylightTime(gc.getTime())) {
    gc.add(Calendar.DATE, 1);
    }*/
    }

    /**
     * Determines if the year is a leap year.
     * Returns true if the year is a leap year.
     */
    public boolean isLeapYear() {
        return isLeapYear(get(Calendar.YEAR));
    }
}
