/**
 * ****************************************************************************
 *
 * Sugar for Java, 2.0.0 Copyright (C) 2001-2007 Dipl.-Inf. (FH) Johann Nepomuk
 * Loefflmann, All Rights Reserved, http://www.jonelo.de
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author jonelo@jonelo.de
 *
 ****************************************************************************
 */
package jonelo.sugar.util;

import java.io.*;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import jonelo.sugar.bytes.Finding;
import jonelo.sugar.bytes.GeneralBytes;

public final class GeneralProgram {

    /**
     * Exits if the JVM does not fulfil the requirements, does nothing under a
     * free JVM
     *
     * @param version Java version (e. g. "1.3.1")
     */
    public static void requiresMinimumJavaVersion(final String version) {
        try {
            String ver = System.getProperty("java.version");
            // no java check under non J2SE-compatible VMs
            if (isJ2SEcompatible() && (ver.compareTo(version) < 0)) {
                System.out.println("ERROR: a later Java Runtime Environment is required."
                        + "\nVendor of Java:                " + System.getProperty("java.vendor")
                        + "\nVersion of Java:               " + ver
                        + "\nRequired minimum Java version: " + version);

                // let's shut down the entire VM
                // no suitable Java VM has been found
                System.exit(1);
            }
        } catch (Throwable t) {
            System.out.println("uncaught exception: " + t);
            t.printStackTrace();
        }
    }

    public static boolean isSupportFor(String version) {
        return isJ2SEcompatible()
                ? (System.getProperty("java.version").compareTo(version) >= 0)
                : false;
    }

    // deprecated
    public static boolean isJ2SEcompatible() {
        String vendor = System.getProperty("java.vendor");
        if ( // gij (http://www.gnu.org/software/classpath)
                vendor.startsWith("Free Software Foundation")
                || // kaffe (http://kaffe.org)
                vendor.startsWith("Kaffe.org")
                || // CACAO (http://www.cacaojvm.org)
                vendor.startsWith("CACAO Team")) {
            return false;
        }
        return true;
    }

    public static boolean isJavaSEcompatible() {
        return isJ2SEcompatible();
    }

    public static String getTimeZoneDataVersion() throws IOException, Exception {

        // tzdata2007a for example, only the first finding is of interest
        String timeZoneDirName = getTimeZoneDirName();
        if (timeZoneDirName != null) { // JDK 1.4.2, JDK 5, JDK 6, JDK 7
            Finding finding = GeneralBytes.search(timeZoneDirName + "ZoneInfoMappings",
                    "tzdata".getBytes(), 5);
            if (finding == null) {
                // OpenJDK 6 doesn't have tzdata in the header, so let's search for
                // the 20.. (yes, that's dirty, but it should work)
                finding = GeneralBytes.search(timeZoneDirName + "ZoneInfoMappings",
                        "20".getBytes(), 3);
            }
            if (finding == null) {
                throw new Exception("tzdata version cannot be determined.");
            }
            String temp = new String(finding.getSequence());
            if (!temp.startsWith("tzdata")) {
                temp = "tzdata" + temp;
            }
            return temp;
        } else {
            // JDK 8 final
            String filenameAbsolute = findTimeZoneJarFile("tzdb.dat");
            if (filenameAbsolute != null) {
                Finding finding = readTimeZoneDatFile(filenameAbsolute);
                return "tzdata" + new String(finding.getSequence());
            }

            // JDK 8 build 1.8.0-ea-b80
            filenameAbsolute = findTimeZoneJarFile("tzdb.jar");
            if (filenameAbsolute != null) {
                Finding finding = readTimeZoneJarFile(filenameAbsolute);
                return "tzdata" + new String(finding.getSequence());
            }
        }
        throw new Exception("Timezone database file hasn't been found.");
    }

    private static Finding readTimeZoneDatFile(String filename) throws Exception {

        Finding finding = null;
        InputStream is = null;
        try {
            is = new FileInputStream(filename);
            finding = GeneralBytes.search(is, "20".getBytes(), 3);
        } catch (Exception e) {
            System.err.println(e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                    System.err.println(e);
                }
            }
        }
        if (finding == null) {
            throw new Exception("tzdata version cannot be determined.");
        }
        return finding;

    }

    // pre JDK 8 final
    private static Finding readTimeZoneJarFile(String filename) throws Exception {

        Finding finding = null;

        ZipFile zipFile = null;
        try {
            zipFile = new ZipFile(filename);
            Enumeration entries = zipFile.entries();

            final String TZDB = "TZDB.dat";
            while (entries.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry) entries.nextElement();
                String entryName = zipEntry.getName();
                if (entryName.equals(TZDB)) {
                    if (zipEntry.getSize() > 0) {
                        InputStream is = zipFile.getInputStream(zipEntry);
                        if (is == null) {
                            throw new Exception("Resource " + TZDB + " was not found.");
                        }
                        finding = GeneralBytes.search(is, "20".getBytes(), 3);
                    }
                    break; // we have found the file that we want
                }

            }//while
        } catch (Exception e) {
            System.err.println(e);
            //e.printStackTrace();
        } finally {
            if (zipFile != null) {
                try {
                    zipFile.close();
                } catch (Exception e) {
                    System.err.println(e);
                }
            }
        }

        if (finding == null) {
            throw new Exception("tzdata version cannot be determined.");
        }
        return finding;
    }

    private static String findTimeZoneJarFile(String filename) {
        String prefix = System.getProperty("java.home") + File.separator;
        String jre = "jre" + File.separator;
        String postfix = "lib" + File.separator + filename;

        String test1 = prefix + jre + postfix;
        File file1 = new File(test1);
        if (file1.isFile()) {
            return test1;
        } else {
            String test2 = prefix + postfix;
            File file2 = new File(test2);
            if (file2.isFile()) {
                return test2;
            }
        }
        return null;
    }

    private static String getTimeZoneDirName() {
        String prefix = System.getProperty("java.home") + File.separator;
        String jre = "jre" + File.separator;
        String postfix = "lib" + File.separator + "zi" + File.separator;

        String test1 = prefix + jre + postfix;
        File file1 = new File(test1);
        if (file1.isDirectory()) {
            return test1;
        } else {
            String test2 = prefix + postfix;
            File file2 = new File(test2);
            if (file2.isDirectory()) {
                return test2;
            }
        }
        return null;
    }
}
