/**
 * ****************************************************************************
 *
 * Sugar for Java, 2.0.0
 * Copyright (C) 2001-2007  Dipl.-Inf. (FH) Johann Nepomuk Loefflmann,
 * All Rights Reserved, http://www.jonelo.de
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @author jonelo@jonelo.de
 *
 ****************************************************************************
 */
package jonelo.sugar.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GeneralNet {

    /**
     * Creates a new instance of GeneralNet
     */
    public GeneralNet() {
    }

    // downloads a url as text
    public static String downloadAsText(String myurl) throws
            MalformedURLException, IOException {
        return downloadAsText(myurl, 16);
    }

    // buffersize can be adjusted to increase performance
    public static String downloadAsText(String myurl, int buffersize) throws
            MalformedURLException, IOException {

        StringBuilder sb = new StringBuilder(buffersize);

        // Create a URL for the desired page
        URL url = new URL(myurl);

        // Read all the text returned by the server
        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
        String str;
        while ((str = in.readLine()) != null) {
            // str is one line of text; readLine() strips the newline character(s)
            sb.append(str);
            sb.append("\n");
        }
        in.close();
// System.out.println("len="+sb.toString().length());
        return sb.toString();
    }


    public static String whatIsTheLatestTzdata() throws Exception {

        String LINK = "https://www.iana.org/time-zones";
        String tzdata = null;
        try {
            String content = downloadAsText(LINK, 8192); // 8k

            // <td><a href="/time-zones/repository/releases/tzdata2016j.tar.gz">tzdata2016j.tar.gz</a> (313.7kb)</td>
            Pattern pattern = Pattern.compile("/(tzdata\\d{4}.)\\.tar\\.gz");

            Matcher matcher = pattern.matcher(content);
            if (matcher.find()) {
                tzdata = matcher.group(1);
            } else {
                throw new Exception("Latest tzdata information cannot be determined from " + LINK);
            }

        } catch (IOException ioe) {
            throw new Exception("Cannot connect to " + LINK);
        }
        if (tzdata == null) {
            throw new Exception(LINK + " doesn't provide tzdata information (anymore).");
        }
        return tzdata;
    }

    /*
-rw-r--r--    1 49       49            129 Oct 24 17:10 README
drwxr-xr-x    2 49       49           4096 Oct 24 17:10 code
drwxr-xr-x    2 49       49           4096 Oct 24 17:10 data
drwxr-xr-x    2 49       49          20480 Oct 24 17:10 releases
lrwxrwxrwx    1 49       49             27 Oct 24 17:13 tzcode-latest.tar.gz -> releases/tzcode2011i.tar.gz
lrwxrwxrwx    1 49       49             27 Oct 24 17:14 tzdata-latest.tar.gz -> releases/tzdata2011m.tar.gz
tzdata-late
     */
    public static String whatIsTheLatestTzdata_old() throws Exception {

        // http://www.iana.org/time-zones
        final String LINK = "ftp://ftp.iana.org/tz/";
        String tzdata = null;
        try {
            String content = downloadAsText(LINK, 800); // 556 for tzdata2007i
            String[] lines = content.split("\n");
            content = null; // we don't need the reference anymore, release it for the GC

            Pattern pattern = Pattern.compile("(tzdata\\d{4}.)");
            boolean found = false;
            int i = 0;
            while (i < lines.length && !found) {
// System.out.println(i+" "+found+" "+lines[i]);
                int index = lines[i].indexOf("tzdata-latest.tar.gz");
                if (index > 0) {

                    Matcher matcher = pattern.matcher(lines[i]);
                    if (matcher.find()) {
                        tzdata = matcher.group(1);
                    } else {
                        throw new Exception("tzdata information cannot be determined from " + LINK);
                    }
                    found = true;
                }
                i++;
            }
        } catch (IOException ioe) {
            throw new Exception("Cannot connect to " + LINK);
        }
        if (tzdata == null) {
            throw new Exception(LINK + " doesn't provide tzdata information.");
        }
        return tzdata;
    }

}
