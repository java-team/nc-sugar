/*
 * GeneralBytes.java
 *
 * Created on 10. Januar 2007, 21:14
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package jonelo.sugar.bytes;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

/**
 * @author jonelo
 */
public class GeneralBytes {
    
    public final static int UNLIMITED = -1;
    
    /** Creates a new instance of GeneralBytes */
    public GeneralBytes() {
    }
    
    // return null if nothing has been found
    public static Finding search(String filename, byte[] bytesToFind, int additional) 
    throws FileNotFoundException, IOException {
        // only one finding
        Findings findings = search(filename, bytesToFind, additional, 1);
        if (findings.size() == 0) return null;
        return findings.get(0);
    }
    
    public static Finding search(InputStream in, byte[] bytesToFind, int additional) 
    throws FileNotFoundException, IOException {
        // only one finding
        Findings findings = search(in, bytesToFind, additional, 1);
        if (findings.size() == 0) return null;
        return findings.get(0);        
    }
    
    // combines array a+b and return it in c
    public static byte[] cat(byte[] a, byte[] b) {
        byte[] c = new byte[a.length+b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }
    
    public static Findings search(InputStream in, byte[] bytesToFind, int additional, int limit)
            throws FileNotFoundException, IOException {
        Findings findings = new Findings();
        byte[] infobytes = new byte[additional];
        
        // Transfer bytes from in to out
        byte[] buf = new byte[4096];
        int numRead;
        
        int index = 0;
        long offset = 0;
        long found = 0;
        while ((numRead = in.read(buf)) > 0) {
            for (int i=0; i < numRead; i++) {
                if (buf[i] == bytesToFind[index]) {
                    index++;
                } else {
                    index = 0;
                }
                // found
                if (index == bytesToFind.length) {
                    long offsetFound = offset+i+1-bytesToFind.length;
                    for (int a = 0; a < additional; a++) {
                        infobytes[a]=buf[i+1+a];
                    }
                    
                    index = 0;
                    found++;
                    findings.add(new Finding(offsetFound, GeneralBytes.cat(bytesToFind,infobytes)));
                    
                    if ((limit != UNLIMITED) && (found >= limit)) {
                        in.close();
                        return findings;
                    }
                }
            }
            offset += numRead;
        }
        in.close();        
        return findings;
        
    }
    
    // ("ZoneInfoMappings", "tzdata".getBytes(), 5, 1);
    public static Findings search(String filename, byte[] bytesToFind, int additional, int limit)
        throws FileNotFoundException, IOException {
        
        InputStream in = new FileInputStream(filename);
        return search (in, bytesToFind, additional, limit);
    }
    
    public static String bytes2hex(byte[] bytes) {
        StringBuilder outBuffer = new StringBuilder();
        for(int x=0; x < bytes.length; x++){
            byte by = bytes[x];
            int i = (by>=0)?by:(256+by);
            
            outBuffer.append(charFromHexDigit(i/16));
            outBuffer.append(charFromHexDigit(i%16));
        }
        return outBuffer.toString().toUpperCase(Locale.US);
    }
    
    
    private static char charFromHexDigit(int digit){
        if((digit >= 0) && (digit <= 9))
            return (char)(digit + '0');
        else
            return (char)(digit - 10 + 'a');
    }
    
    public static String bytes2text(byte[] bytes) {
        StringBuilder outBuffer = new StringBuilder();
        for(int x=0; x < bytes.length; x++){
            char v = (char)bytes[x];
            if((v >= ' ') && (v < (char)0x7f)){
                outBuffer.append(v);
            } else {
                outBuffer.append('.');
            }
        }
        return outBuffer.toString();
    }
    
    public static byte[] hex2bytes(String sequence) throws NumberFormatException {
        // a hex sequence is expected
        byte[] bytearr = null;
        
        if ((sequence.length() %2 )==1) {
            throw new NumberFormatException("An even number of nibbles was expected.");
        }
        
        bytearr = new byte[sequence.length() / 2];
        int x = 0;
        for (int i=0; i < sequence.length();) {
            String str = sequence.substring(i,i+=2);
            bytearr[x++]=(byte)Integer.parseInt(str,16);
        }
        return bytearr;
    }
    
    
}
