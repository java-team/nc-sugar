/*
 * Findings.java
 *
 * Created on 10. Januar 2007, 21:21
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package jonelo.sugar.bytes;

import java.util.Vector;

/**
 *
 * @author jonelo
 */
public class Findings {
    
    private Vector v;

    /**
     * Creates a new instance of Findings
     */
    public Findings() {
        v = new Vector();
    }
    
    public void add(Finding finding) {
        v.add(finding);
    }
    
    public void clear() {
        v.clear();
    }
    
    public int size() {
        return v.size();
    }
    
    public Finding get(int i) {
        return (Finding)v.get(i);
    }
   
    public void print() {
       System.out.println("Offset\t\tSequence (hex)\tSequence (txt)");
       for (int i=0; i < v.size(); i++) {
           ((Findings)v.get(i)).print();
       }
       System.out.println("Found: "+v.size());
    }
}
