/*
 * Finding.java
 *
 * Created on 10. Januar 2007, 21:19
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package jonelo.sugar.bytes;

import java.util.Locale;

/**
 *
 * @author jonelo
 */
public class Finding {
    
    private long offset;
    private byte[] sequence;
    
    /** Creates a new instance of Finding */
    public Finding(long offset, byte[] sequence) {
        this.offset = offset;
        this.sequence = sequence;
    }

    public long getOffset() {
        return offset;
    }

    public void setOffset(long offset) {
        this.offset = offset;
    }

    public byte[] getSequence() {
        return sequence;
    }

    public void setSequence(byte[] sequence) {
        this.sequence = sequence;
    }
    
    public void print() {
        System.out.println(toString());
    }
    
    public String toString() {
        return
          "0x" +
          Long.toString(offset, 16).toUpperCase(Locale.US) +
          "\t\t" +
          GeneralBytes.bytes2hex(sequence) +
          "\t" +
          GeneralBytes.bytes2text(sequence);
    }
}
